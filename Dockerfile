FROM swaggerapi/swagger-ui
LABEL "Vendor"="Nwave"
LABEL "Service"="swagger_dm"
ENV SWAGGER_JSON=/opt/dm.json
ADD dm.json /opt/
